package com.techuniversity.apitechudb.controllers;

import com.techuniversity.apitechudb.models.PurchaseModel;
import com.techuniversity.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("PurchaseController.getPurchases");
        return new ResponseEntity<>(purchaseService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/purchases")
    public ResponseEntity<?> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("PurchaseController.addPurchase: " + purchase.toString());
        Object result = purchaseService.add(purchase);
        HttpStatus status;
        if (result.equals("User not found") || result.equals("Product not found")) status = HttpStatus.NOT_FOUND;
        else if (result.equals("Purchase already exists")) status = HttpStatus.CONFLICT;
        else status = HttpStatus.CREATED;

        return new ResponseEntity<>(result, status);
    }

    @DeleteMapping("/purchases/{id}")
    public ResponseEntity<?> deletePurchase(@PathVariable String id) {
        System.out.println("PurchaseController.deletePurchase: " + id);
        purchaseService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // Prefiero no comunicarle al consumidor si el usuario existía o no
    }
}
