package com.techuniversity.apitechudb.controllers;

import com.techuniversity.apitechudb.models.ProductModel;
import com.techuniversity.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("ProductController.getProducts");
        return new ResponseEntity<>(productService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> getProductById(@PathVariable String id) {
        System.out.println("ProductController.getProductById: " + id);
        Optional<ProductModel> result = productService.findById(id);
        return new ResponseEntity<>(result.isPresent() ? result.get() : "Producto no encontrado", result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("ProductController.addProduct: " + product.toString());
        return new ResponseEntity<>(productService.add(product), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@PathVariable String id, @RequestBody ProductModel product) {
        System.out.println("ProductController.updateProduct: id: " + id + " product: " + product.toString());
        return new ResponseEntity<>(product, productService.update(id, product) != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable String id) {
        System.out.println("ProductController.deleteProduct: " + id);
        productService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // Prefiero no comunicarle al consumidor si el producto existía o no
    }

}
