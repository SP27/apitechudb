package com.techuniversity.apitechudb.controllers;

import com.techuniversity.apitechudb.models.UserModel;
import com.techuniversity.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})  // Es para el CORS de los navegadores
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(required = false, name = "$orderby") String orderBy) {
        System.out.println("UserController.getUsers");
        return new ResponseEntity<>(userService.findAll(orderBy), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUserById(@PathVariable String id) {
        System.out.println("UserController.getUserById: " + id);
        Optional<UserModel> result = userService.findById(id);
        return new ResponseEntity<>(result.isPresent() ? result.get() : "Usuario no encontrado", result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("UserController.addUser: " + user.toString());
        return new ResponseEntity<>(userService.add(user), HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@PathVariable String id, @RequestBody UserModel user) {
        System.out.println("UserController.updateUser: id: " + id + " user: " + user.toString());
        return new ResponseEntity<>(user, userService.update(id, user) != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable String id) {
        System.out.println("UserController.deleteUser: " + id);
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // Prefiero no comunicarle al consumidor si el usuario existía o no
    }
}
