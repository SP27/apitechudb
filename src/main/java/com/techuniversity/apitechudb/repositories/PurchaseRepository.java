package com.techuniversity.apitechudb.repositories;

import com.techuniversity.apitechudb.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}
