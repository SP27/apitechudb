package com.techuniversity.apitechudb.services;

import com.techuniversity.apitechudb.models.ProductModel;
import com.techuniversity.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("ProductService.findAll");
        return productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("ProductService.findById: " + id);
        return productRepository.findById(id);
    }

    public ProductModel add(ProductModel product) {
        System.out.println("ProductService.add: " + product.toString());
        return productRepository.save(product);
    }

    public ProductModel update(String id, ProductModel product) {
        System.out.println("ProductService.update: id: " + id + " product: " + product.toString());
        if (findById(id).isPresent()) return productRepository.save(product);
        else return null;
    }

    public void delete(String id) { // Prefiero no informar al consumidor sobre si el producto existía o no
        System.out.println("ProductService.delete: " + id);
        productRepository.deleteById(id);
    }
}
