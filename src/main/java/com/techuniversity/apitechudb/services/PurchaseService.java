package com.techuniversity.apitechudb.services;

import com.techuniversity.apitechudb.models.ProductModel;
import com.techuniversity.apitechudb.models.PurchaseModel;
import com.techuniversity.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    public List<PurchaseModel> findAll() {
        System.out.println("PurchaseService.findAll");
        return purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("PurchaseService.findById: " + id);
        return purchaseRepository.findById(id);
    }

    public Object add(PurchaseModel purchase) {
        System.out.println("PurchaseService.add: " + purchase.toString());

        if (findById(purchase.getId()).isPresent()) return "Purchase already exists";
        if (!userService.findById(purchase.getUserId()).isPresent()) return "User not found";
        float totalAmount = 0;

        if (purchase.getPurchaseItems() != null && !purchase.getPurchaseItems().isEmpty()) {
            for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
                Optional<ProductModel> foundProduct = productService.findById(purchaseItem.getKey());
                if (!foundProduct.isPresent()) return "Product not found";
                else totalAmount += foundProduct.get().getPrice() * purchaseItem.getValue();
            }
        }

        purchase.setAmount(totalAmount);
        return purchaseRepository.insert(purchase);
    }

    public void delete(String id) { // Prefiero no informar al consumidor sobre si el usuario existía o no
        System.out.println("PurchaseService.delete: " + id);
        purchaseRepository.deleteById(id);
    }
}
