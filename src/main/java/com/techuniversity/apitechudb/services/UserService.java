package com.techuniversity.apitechudb.services;

import com.techuniversity.apitechudb.models.UserModel;
import com.techuniversity.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String orderBy) {
        System.out.println("UserService.findAll");
        if (orderBy != null && orderBy.equalsIgnoreCase("age")) return userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
        return userRepository.findAll();
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("UserService.findById: " + id);
        return userRepository.findById(id);
    }

    public UserModel add(UserModel user) {
        System.out.println("UserService.add: " + user.toString());
        return userRepository.save(user);
    }

    public UserModel update(String id, UserModel user) {
        System.out.println("UserService.update: id: " + id + " user: " + user.toString());
        if (findById(id).isPresent()) {
            user.setId(id);
            return userRepository.save(user);
        }
        else return null;
    }

    public void delete(String id) { // Prefiero no informar al consumidor sobre si el usuario existía o no
        System.out.println("UserService.delete: " + id);
        userRepository.deleteById(id);
    }
}
